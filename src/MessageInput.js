import React, { Component } from 'react';
import './App.css';

class MessageInput extends Component {
	render() {
		return (
			<div className="input">
            	<textarea placeholder="Escribí tu mensaje acá" value={this.props.textValue} onChange={this.props.textChange} />
            	<button value="button" id="send" onClick={this.props.sendMessage}>Send</button>
          </div>
	)}
}

export default MessageInput; 
