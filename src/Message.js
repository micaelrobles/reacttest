import React, { Component } from 'react';
import './App.css';

class Message extends Component {
	render() {
		return (
			<div className="message" key={this.props.ID}>
		        <h4 className="messagename">{this.props.messageUser}</h4>
		        <p className="messagetext">{this.props.messageText}</p>
	        </div>
	)}
}

export default Message; 
