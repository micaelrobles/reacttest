import React from 'react';
import ReactDOM from 'react-dom';
import Window from './Window';
import './index.css';

ReactDOM.render(
  <div className="container"><Window /></div>,
  document.getElementById('root')
);
