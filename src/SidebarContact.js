
import React, { Component } from 'react';
import './App.css';

class SidebarContact extends Component {
	render() {
		return (
			<div className={this.props.className} key={this.props.ID} onClick={this.props.event}>
		        <p className="sidebarContactName">{this.props.contactName}</p>
		        <p className="sidebarContactMessage">{this.props.lastText}</p>
	        </div>
	)}
}

export default SidebarContact; 
