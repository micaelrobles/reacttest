
import React, { Component } from 'react'; 
import './App.css'; 
import Message from './Message'
import MessageInput from './MessageInput'
import SidebarContact from './SidebarContact'


class Window extends Component { 
 
    constructor() {
        super();
        this.getLastMessageText = this.getLastMessageText.bind(this);
        this.getLastMessageUserID = this.getLastMessageUserID.bind(this);
        this.getLastMessageID = this.getLastMessageID.bind(this);

        this.sendMessage = this.sendMessage.bind(this);
        this.textChange = this.textChange.bind(this);

        this.changeSelectedChat = this.changeSelectedChat.bind(this);

        this.state = { 
            textValue: "",
            selectedChat: 42,
            inbox: { 
                42: { 
                      1: {msgID: 1, userID: 42, messageText: "Hola"},
                      2: {msgID: 2, userID: 42, messageText: "Estas probando mi primer app con React?"},
                      3: {msgID: 3, userID: 42, messageText: "Intentá entrar a otros chats!"}
                    },

                 12: { 
                        1: {msgID: 1, userID: 12, messageText: "Hola"},
                    } 
                }, 

            contacts: { 
                0: {
                  name: "Micael Robles"
                },
                42: { 
                  name: "Nacho Raso" 
                }, 
                12: { 
                  name: "Ignacio Martín" 
                } 
            } 
        }; 
    } 

  getLastMessageText(id){
    const lastMsg = this.state.inbox[id][Object.keys(this.state.inbox[id])
    .sort((a,b) => a + b)[0]]
    .messageText;

    return lastMsg;
  }

  getLastMessageUserID(id){
    console.log(this.state.inbox)
    const lastMsgUserId = this.state.inbox[id][Object.keys(this.state.inbox[id])
    .sort((a,b) => a + b)[0]]
    .userID;

    return lastMsgUserId;
  }

  getLastMessageID(id){
    const lastMsgId = Object.keys(this.state.inbox[id]).sort((a,b) => a + b)[0];
    return lastMsgId;
  }

  textChange(event){
    this.setState({
      textValue: event.target.value
    })
  }

  sendMessage() {
    if (this.state.textValue){

    let newMsgID = Number(this.getLastMessageID(this.state.selectedChat))+1;

    this.setState({
      textValue: "",
      inbox: Object.assign({}, this.state.inbox, {
          [this.state.selectedChat]: Object.assign({}, this.state.inbox[this.state.selectedChat], {
            [newMsgID]: {msgID: newMsgID, userID: 0, messageText: this.state.textValue}
          })
      })

      })}
  }; 

  changeSelectedChat(id) {
    this.setState({
      selectedChat: id
    })
  }


  
 
  render() { 

    const sidebarContent = Object.keys(this.state.inbox).map((ID) => {
        
        if (ID == this.state.selectedChat){
          return <SidebarContact key={ID} contactName={this.state.contacts[ID].name} lastText={this.state.inbox[ID][this.getLastMessageID(ID)].messageText} event={() => this.changeSelectedChat(ID)} className="sidebarcontact selected" />
        } else {
          return <SidebarContact key={ID} contactName={this.state.contacts[ID].name} lastText={this.state.inbox[ID][this.getLastMessageID(ID)].messageText} event={() => this.changeSelectedChat(ID)} className="sidebarcontact" />
        }

      });

    const messageWindow = Object.keys(this.state.inbox[this.state.selectedChat]).map((ID) =>
      <Message messageUser={this.state.contacts[this.state.inbox[this.state.selectedChat][ID].userID].name} messageText={this.state.inbox[this.state.selectedChat][ID].messageText} key={ID} />
      );
  
    //<MessageInput textValue={this.state.TextValue} sendMessage={this.sendMessage} textChange={this.props.textChange} />

    return ( 
      <div>
        <div className="sidebar">{sidebarContent}</div>
        <div className="rightside">
        <div className="messagewindow">
          {messageWindow}
        </div>
        <MessageInput textValue={this.state.textValue} sendMessage={this.sendMessage} textChange={this.textChange} />
        </div>
      </div>
    ); 
  } 
} 


export default Window; 